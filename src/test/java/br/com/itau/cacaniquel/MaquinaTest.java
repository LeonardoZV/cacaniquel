package br.com.itau.cacaniquel;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;

public class MaquinaTest {

	private Maquina maquina;
	
	@Before
	public void preparar() {
		maquina = new Maquina();
	}
	
	@Test
	public void deveInstanciarMaquinaCorretamente() {
		Assert.assertEquals(4, maquina.getSimbolos().size());
	}
	
	@Test
	public void deveRetornarTresSimbolosAleatorios() {
		List<Simbolo> simbolos = maquina.obterTresSimbolosAleatorios();
		Assert.assertEquals(3, simbolos.size());
	}
	
	@Test
	public void devePontuarCorretamenteQuandoOsTresSimbolosSaoDiferentes() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.LARANJA);
		simbolos.add(Simbolo.MACA);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(80, pontuacao);
	}
	
	@Test
	public void devePontuarCorretamenteQuandoDoisSimbolosSaoIguais1() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.MACA);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(80, pontuacao);
	}
	
	@Test
	public void devePontuarCorretamenteQuandoDoisSimbolosSaoIguais2() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.MACA);
		simbolos.add(Simbolo.BANANA);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(80, pontuacao);
	}
	
	@Test
	public void devePontuarCorretamenteQuandoDoisSimbolosSaoIguais3() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.MACA);
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.BANANA);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(80, pontuacao);
	}
	
	@Test
	public void devePontuarCorretamenteQuandoTresSimbolosSaoIguaisEDiferentesDeSete() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.BANANA);
		simbolos.add(Simbolo.BANANA);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(90, pontuacao);
	}
	
	@Test
	public void devePontuarCorretamenteQuandoTresSimbolosSaoIguaisEIguaisASete() {
		List<Simbolo> simbolos = new ArrayList<Simbolo>();
		simbolos.add(Simbolo.SETE);
		simbolos.add(Simbolo.SETE);
		simbolos.add(Simbolo.SETE);
		int pontuacao = maquina.calcularPontuacao(simbolos);
		Assert.assertEquals(1500, pontuacao);
	}
}
