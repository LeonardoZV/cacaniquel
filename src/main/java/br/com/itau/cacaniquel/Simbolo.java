package br.com.itau.cacaniquel;

public enum Simbolo {
	BANANA(10),
	MACA(20),
	LARANJA(50),
	SETE(100);
	
	private int pontuacao;
	
	Simbolo(int pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	public int getPontuacao() {
		return pontuacao;
	}
}
