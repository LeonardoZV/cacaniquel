package br.com.itau.cacaniquel;

import java.util.List;

public class Console {

	public static void exibirSimbolos(List<Simbolo> simbolos) {
		for(Simbolo simbolo : simbolos) {
			System.out.print(simbolo.toString() + " ");
		}
	}
	
	public static void exibirPontuacao(int pontuacao) {
		System.out.println("= " + pontuacao);
	}
}
