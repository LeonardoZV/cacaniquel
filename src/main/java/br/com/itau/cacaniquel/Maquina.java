package br.com.itau.cacaniquel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Maquina {
	
	private List<Simbolo> simbolosPossiveis;
	
	public Maquina() {
		simbolosPossiveis = new ArrayList<Simbolo>();
		for(Simbolo simbolo : Simbolo.values()) {
			simbolosPossiveis.add(simbolo);
		}
	}
	
	public List<Simbolo> getSimbolos() {
		return simbolosPossiveis;
	}
	
	public List<Simbolo> obterTresSimbolosAleatorios() {
		Random gerador = new Random();
		List<Simbolo> simbolosRetirados = new ArrayList<Simbolo>();		
		for(int i = 0; i < 3;i++) {
			simbolosRetirados.add(simbolosPossiveis.get(gerador.nextInt(4)));
		}
		return simbolosRetirados;
	}
	
	public int calcularPontuacao(List<Simbolo> simbolos) {
		Simbolo primeiro = simbolos.get(0);
		Simbolo segundo = simbolos.get(1);
		Simbolo terceiro = simbolos.get(2);
		
		if(primeiro == segundo && primeiro == terceiro) {
			if (primeiro == Simbolo.SETE)
				return (primeiro.getPontuacao() + segundo.getPontuacao() + terceiro.getPontuacao())*5;
			else
				return (primeiro.getPontuacao() + segundo.getPontuacao() + terceiro.getPontuacao())*3;
		}
		else {
			if(primeiro == segundo && segundo != terceiro || primeiro == terceiro && segundo != terceiro || segundo == terceiro && primeiro != segundo)
				return (primeiro.getPontuacao() + segundo.getPontuacao() + terceiro.getPontuacao())*2;
			else
				return primeiro.getPontuacao() + segundo.getPontuacao() + terceiro.getPontuacao();
		}
	}
	
	public void jogar() {
		List<Simbolo> simbolosAleatorios = obterTresSimbolosAleatorios();
		int pontuacao = calcularPontuacao(simbolosAleatorios);
		Console.exibirSimbolos(simbolosAleatorios);
		Console.exibirPontuacao(pontuacao);
	}
}
